let store = chrome.storage.sync
let storeOk = await store.get([])
if (storeOk) {
    console.log("using 'sync' storage")
} else {
    store = chrome.storage.local
    console.log("using 'local' storage")
}

const config = document.getElementById("config");
await store.get("config")
    .then(items => items.config)
    .then(data => config.value = data || "# No rules loaded from storage.");
document.getElementById("config-save").addEventListener("click", async () => await store.set({config: config.value}));
document.getElementById("config-sort").addEventListener("click", () => {
    // Sort lines in blocks separated by comments and empty lines.
    var i, l, line, lines = config.value.split("\n"), block = [], whole = [];
    for (i = 0, l = lines.length; i < l; i += 1) {
        line = lines[i].trim();
        if (/^\s*$/.test(line) || /^\s*#/.test(line)) {
            whole = whole.concat(block.sort(compareLines));
            block = [];
            whole.push(line);
        } else {
            block.push(line);
        }
    }
    whole = whole.concat(block.sort(compareLines));
    config.value = whole.join("\n");

    function compareLines(a, b) {
        return a.localeCompare(b);
    }
});

// Observing
let enabled = false;
let results = document.getElementById("results");
let requests = [];

chrome.webRequest.onCompleted.addListener(onRequest, {urls: ["<all_urls>"]});
chrome.webRequest.onErrorOccurred.addListener(onRequest, {urls: ["<all_urls>"]});

function onRequest(details) {
    if (!enabled) return;
    requests.push(details);
    results.appendChild(createResultItem(details));
}

function createResultItem(details) {
    const e = document.createElement("li");
    e.appendChild(document.createTextNode(details.url));
    e.className =
        details.fromCache  ? "cache" :
        details.error      ? "block" :
        details.statusLine ? "allow" :
        "";
    e.title = details.method + " | " + (details.error || details.statusLine)
    return e;
}

document.getElementById("results-toggle").addEventListener("click", (e) => {
    enabled = !enabled;
    e.currentTarget.src = enabled
        ? "options_results_toggle-live.png"
        : "options_results_toggle-dead.png";
});

document.getElementById("results-clear").addEventListener("click", () => {
    while (results.lastChild) results.removeChild(results.lastChild);
    Object.assign(requests, []);
});

document.getElementById("results-sort").addEventListener("click", () => {
    while (results.lastChild) results.removeChild(results.lastChild);
    requests
        .sort(compareRequests)
        .filter((e, i, a) => !i || e.url != a[i-1].url) // remove duplicates
        .map(createResultItem)
        .forEach(e => results.appendChild(e));

    function compareRequests(a, b) {
        return (!a.error && b.error) ? 1 : (a.error && !b.error) ? -1 : a.url.localeCompare(b.url);
    }
});
