# ![logo](icons/reqblock_48.png) Reqblock

A simple request blocker WebExtension.

The blacklist can be specified on the options page.
It is a simple block of text (easy to copy & paste).

# Configuration

Empty lines and lines where the first non-whitespace character is `#` are ignored.
This enables commenting and grouping rules.
The organize button removes leading and trailing whitespace from every line and sorts lines alphabetically in the groups.

Each line is an [`urlFilter` match pattern](https://developer.chrome.com/docs/extensions/reference/api/declarativeNetRequest#property-RuleCondition-urlFilter).
